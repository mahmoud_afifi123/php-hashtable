<?php
class HashClass
{
    public $appended_array=array();
    public $most_frequent=array();
    public static $hashclass;
    
    private function __construct()
    {
        
    }
    
    
    public static function getInstanceFromHashClass()
    {
        if(static::$hashclass == null)
        {
            static::$hashclass = new HashClass();
        }
        return static::$hashclass;
    }
    
    
    public function hashTable($exploded_content)
    {
        
        foreach($exploded_content as $key)
        {
            if(array_key_exists($key,$this->appended_array))
            {
                $this->appended_array[$key] = $this->appended_array[$key] + 1;
            }
            else
            {
                 $this->appended_array[$key] = 1;
            }
        }
        
      var_dump($this->appended_array);
      echo '<br>';    
    }
    
    public function countWords($exploded_content)
    {
        echo '<br>'. count($exploded_content).'<br>';
    }
    public function mostFrequent()
    {
        $current=0;
        $array=array_values($this->appended_array);
        $iterator=0;
        for($i=0 ; $i < count($array) ; $i++)
        {
            $iterator++;
            $tmp=null;
            $current=$array[$i]; 
            if($iterator == count($array))
            {
                break;
            }
            if($array[$i] > $array[$i+1])
            {
                $tmp = $array[$i];
                $array[$i]=$array[$i+1];
                $array[$i+1]=$tmp;
            }
            
        }
        $this->most_frequent=array_keys($this->appended_array,$current);
        //var_dump($this->most_frequent);
        foreach($this->most_frequent as $key)
        {
            echo '<br> word <b>' . $key . '</b> most frequented with ' . $this->appended_array[$key] .' times<br>';
        }
    }
}