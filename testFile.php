<?php
include 'FileClass.php';
include 'HashClass.php';
class testFile{
    
    public $FileClass;
    public $HashClass;
    
    public function getInestances()
    {
        $this->FileClass=FileClass::getInstanceFromFileClass();   
        $this->HashClass=HashClass::getInstanceFromHashClass();   
    }
    
    public function excuteFunctionality($file)
    {
        $exploaded_data=$this->FileClass->explodeData($file);
        
        $this->HashClass->hashTable($exploaded_data);
        
        $this->HashClass->countWords($exploaded_data);
        
        $this->HashClass->mostFrequent();
        
    }
}
$testFile=new testFile();
$testFile->getInestances();
$testFile->excuteFunctionality('test.txt');