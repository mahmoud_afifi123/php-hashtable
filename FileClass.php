<?php
class FileClass
{
    public $file_content;
    public static $fileclass;
    
    
    private function __construct()
    {
        
    }
    public static function getInstanceFromFileClass()
    {
        if(static::$fileclass == null)
        {
            static::$fileclass = new FileClass();
        }
        return static::$fileclass;
    }
    
    public function getData($file)
    {
        $this->file_content=file_get_contents($file);
       
    }
    public function explodeData($file){
       $this->getData($file);
       return $exploded_content=explode(' ',$this->file_content);
    }
    
}
